import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import java.util.*;
import org.hamcrest.MatcherAssert;

public class Desafio2 {
    private WebDriver driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;
    @Before
    public void setUp() {
        driver = new ChromeDriver();
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();
    }
    @After
    public void tearDown() {
        driver.quit();
    }
    @Test
    public void Desafio2() {
        driver.get("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");
        driver.findElement(By.id("switch-version-select")).click();
        {
            WebElement dropdown = driver.findElement(By.id("switch-version-select"));
            dropdown.findElement(By.xpath("//option[. = 'Bootstrap V4 Theme']")).click();
        }
        driver.findElement(By.linkText("Add Record")).click();
        driver.findElement(By.id("field-customerName")).sendKeys("Teste Sicredi");
        driver.findElement(By.id("field-contactLastName")).sendKeys("Teste");
        driver.findElement(By.id("field-contactFirstName")).sendKeys("Leonardo");
        driver.findElement(By.id("field-phone")).sendKeys("51 9999-9999");
        driver.findElement(By.id("field-addressLine1")).sendKeys("Av Assis Brasil, 3970");
        driver.findElement(By.id("field-addressLine2")).sendKeys("Torre D");
        driver.findElement(By.id("field-city")).sendKeys("Porto Alegre");
        driver.findElement(By.id("field-state")).sendKeys("RS");
        driver.findElement(By.id("field-postalCode")).sendKeys("91000-000");
        driver.findElement(By.id("field-country")).sendKeys("Brasil");
        driver.findElement(By.id("field-salesRepEmployeeNumber")).sendKeys("Fixter");
        driver.findElement(By.id("field-creditLimit")).sendKeys("200");
        driver.findElement(By.id("form-button-save")).click();
        driver.findElement(By.id("form-button-save")).click();
        try {
            Thread.sleep(3500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MatcherAssert.assertThat(driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div[15]/div[2]/p")).getText(), containsString("Your data has been successfully stored into the database"));
        driver.findElement(By.cssSelector("#report-success > p > a:nth-child(2)")).click();
        driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/thead/tr[2]/td[3]/input")).click();
        driver.findElement(By.name("customerName")).sendKeys("Teste Sicredi");
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/tbody/tr[1]/td[2]/div[1]/div/button")).click();
        driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/tbody/tr[1]/td[2]/div[1]/div/div/a[3]")).click();
        driver.findElement(By.cssSelector(".delete-confirmation p")).click();
        driver.findElement(By.cssSelector(".delete-confirmation p")).click();
        {
            WebElement element = driver.findElement(By.cssSelector(".delete-confirmation p"));
            Actions builder = new Actions(driver);
            builder.doubleClick(element).perform();
        }
        MatcherAssert.assertThat(driver.findElement(By.cssSelector(".delete-confirmation p")).getText(), is("Are you sure that you want to delete this record?"));
        driver.findElement(By.cssSelector(".delete-confirmation-button")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MatcherAssert.assertThat(driver.findElement(By.xpath("/html/body/div[3]/span[3]/p")).getText(), containsString("Your data has been successfully deleted from the database."));
        driver.close();
    }
}
